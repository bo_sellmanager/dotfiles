#!/usr/bin/env bash
 
# oh my zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

# powerlevel10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# copy dotfiles
cp .p10k.zsh ~/.p10k.zsh
cp .zshrc ~/.zshrc

sudo chsh $USER -s $(which zsh)
zsh
